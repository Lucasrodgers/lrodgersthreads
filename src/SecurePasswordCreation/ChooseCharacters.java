package SecurePasswordCreation;

import java.util.Random;

public class ChooseCharacters {

    public char findRandChar(String passwordCharacters){

        //Selects a character from the character string and returns it
        Random random = new Random();
        int index = random.nextInt(passwordCharacters.length());
        return passwordCharacters.charAt(index);
    }
}
