package SecurePasswordCreation;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SetPasswordLength {
    public static String passwordCharacters= "AaBbCcDdEeFfGgHhIiJjKkLlMm" +
            "NnOoPpQqRrSsTtUuVvWwXxYyZz0123456789!@#$%^&*()_+-";

    public static void main(String[] args) {

        //Get number of characters for password
        Scanner numChar = new Scanner(System.in);
        System.out.println("\nPlease enter how many characters you want your password to be: ");
        int passwordLength;
        passwordLength = Integer.parseInt(numChar.nextLine());
        System.out.println("\nHere is your password that is " + passwordLength + " characters long.");

        //Set the thread pool to be equal to the user password length
        ExecutorService service = Executors.newFixedThreadPool(passwordLength);

        //Initiates an array to store all the thread calls
        ArrayList<PasswordGenerator> newPassword = new ArrayList<>(passwordLength);

        //Adds all the threads needed in the Array List
        for (int i = 0; i < passwordLength ; i++) {
            PasswordGenerator newChar = new PasswordGenerator();
             newPassword.add(newChar);
             service.execute(newPassword.get(i));
         }

        //Ends thread service
        service.shutdown();
    }
}
